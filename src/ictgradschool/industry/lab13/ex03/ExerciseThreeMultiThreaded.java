package ictgradschool.industry.lab13.ex03;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * TODO Create a multi-threaded program to calculate PI using the Monte Carlo method.
 */
public class ExerciseThreeMultiThreaded extends ExerciseThreeSingleThreaded {

    /**
     * Estimates PI using a multi-threaded Monte Carlo method.
     */
    @Override
    protected double estimatePI(long numSamples) {
        // TODO Implement this.
        final int numThreads = 4;
        final long newNumSamples = numSamples/numThreads;
        piThreads[] threads = new piThreads[numThreads];
        for(int i = 0; i < numThreads; i++){
            threads[i] = new piThreads(newNumSamples);
            threads[i].start();
        }
        for(int j = 0; j <numThreads; j++){
        try{
            threads[j].join();
        }catch(InterruptedException e) {
            e.printStackTrace();
        }
        }
        long sum = 0;
        for (int k = 0; k < numThreads; k++){
        sum+= threads[k].getNum();
        }
        double estimatedPi = 4.0 * (double) sum / (double) numSamples;
        return  estimatedPi;
    }

    /** Program entry point. */
    public static void main(String[] args) {
        new ExerciseThreeMultiThreaded().start();
    }
}
