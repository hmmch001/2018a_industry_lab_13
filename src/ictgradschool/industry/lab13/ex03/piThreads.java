package ictgradschool.industry.lab13.ex03;

import java.util.concurrent.ThreadLocalRandom;

public class piThreads extends Thread {
    long numInsideCircle = 0;
    long newNumSamples;

    public piThreads(long newNumSamples){
        this.newNumSamples = newNumSamples;
    }

    public void run() {
        ThreadLocalRandom tlr = ThreadLocalRandom.current();

        for (long j = 0; j < newNumSamples; j++) {

            double x = tlr.nextDouble();
            double y = tlr.nextDouble();

            if (Math.pow(x, 2.0) + Math.pow(y, 2.0) < 1.0) {
                numInsideCircle++;
            }

        }
    }

    public double getNum() {
        return numInsideCircle;
    }
}
