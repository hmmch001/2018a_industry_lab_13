package ictgradschool.industry.lab13.ex04;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ConsumerRunnable implements Runnable {
    final BlockingQueue<Transaction> queue;
    BankAccount account;
    public ConsumerRunnable(BlockingQueue<Transaction> queue, BankAccount account){
        this.queue = queue;
        this.account = account;
    }
    @Override
    public void run() {
        boolean complete = false;
        while(!complete)
        try{
            Transaction transaction = queue.take();
            switch (transaction._type) {
                case Deposit:
                    account.deposit(transaction._amountInCents);
                    break;
                case Withdraw:
                    account.withdraw(transaction._amountInCents);
                    break;
            }
        }catch(InterruptedException e){
            complete = true;
        }

    }
}
