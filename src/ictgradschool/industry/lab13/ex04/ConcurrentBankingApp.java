package ictgradschool.industry.lab13.ex04;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ConcurrentBankingApp {
    private void start() throws InterruptedException {
        final BlockingQueue<Transaction> queue = new ArrayBlockingQueue<>(10);
        BankAccount account = new BankAccount();
        Thread producer = new Thread(new Runnable() {
            @Override
            public void run(){
                List<Transaction> transactions = TransactionGenerator.readDataFile();
                for(int i = 0; i<transactions.size(); i++){
                    try {
                        queue.put(transactions.get(i));
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }
                }
            }
        });
        Thread consumer1 =  new Thread(new ConsumerRunnable(queue, account));
        Thread consumer2 =  new Thread(new ConsumerRunnable(queue, account));
        producer.start();
        consumer1.start();
        consumer2.start();
        producer.join();
        consumer1.interrupt();
        consumer2.interrupt();
        consumer1.join();
        consumer2.join();

        System.out.println(account.getFormattedBalance());
    }
    public static void main(String[] args) throws InterruptedException {
        new ConcurrentBankingApp().start();
    }

}
