package ictgradschool.industry.lab13.ex05;

import ictgradschool.Keyboard;

public class CommandLine {
    public void start() throws InterruptedException{
        System.out.println("Enter a value: ");
        long n = Long.parseLong(Keyboard.readInput());
        PrimeFactorsTask factors = new PrimeFactorsTask(n);
        Thread calculation = new Thread(factors);
        Thread abort = new Thread(new Runnable(){
            @Override
            public void run() {
                System.out.println("Calculating, press A to abort");
                String userInput = Keyboard.readInput();
                if(userInput.equals("A")){
                    calculation.interrupt();
                }
            }
        });
        calculation.start();
        abort.start();
        calculation.join();

        if(factors.getState() == PrimeFactorsTask.TaskState.Aborted){
            System.out.println("Aborted");
        }else{
            System.out.println("Prime factors: ");
            System.out.println(factors.getPrimeFactors());
        }
    }

    public static void main(String[] args) throws InterruptedException {

        new CommandLine().start();
    }
}
