package ictgradschool.industry.lab13.ex05;

import ictgradschool.industry.lab13.examples.example03.IComputeTask;

import java.util.ArrayList;
import java.util.List;

public class PrimeFactorsTask implements Runnable {

        private long n;
        public enum TaskState{Initialized, Completed, Aborted}
        List<Long> factors = new ArrayList<>();
        TaskState state = TaskState.Initialized;

        public PrimeFactorsTask(long n) {
            this.n = n;
        }

        public void run() {


            long n = this.n;


                for (long i = 2; i * i <= n; i++) {
                    if (!Thread.currentThread().isInterrupted()) {


                        while (n % i == 0) {
                            factors.add(i);
                            n = n / i;
                        }
                        state = TaskState.Completed;
                    }else{
                        state = TaskState.Aborted;
                        break;
                    }
                }
                if (n > 1) {
                    factors.add(n);
                }


        }

        public long n(){
            return n;

        }

        public List<Long> getPrimeFactors() throws IllegalStateException{
            return factors;
        }

        public TaskState getState(){
            return state;
        }


}




